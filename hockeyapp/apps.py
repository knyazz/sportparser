#coding: utf-8
from __future__ import unicode_literals
from django.apps import AppConfig


class HockeyAppConfig(AppConfig):
    name = 'hockeyapp'
    verbose_name = u'Хоккейная статистика'