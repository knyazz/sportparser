INSTALL

activate virtualenv and install requirements.

```
pip install -r requirements.txt
./manage.py migrate
./manage.py createsuperuser
./manage.py runserver
```